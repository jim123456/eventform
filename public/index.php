<?php session_start()?>
<!DOCTYPE html>
<html lang="en">
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.png">

    <title>Event form | Register for a free conference</title>

    <!--TailwindCss-->
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="eventform.css" rel="stylesheet">
    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Lato|Lobster|Playfair+Display: 400,900;" rel="stylesheet">
    
  </head>
  
   

    <body class="home-body">

      <div id="page-title" class="playfair-font-black large-letters text-center mb-10 p-4">
        Sign up for a free conference.
        
      </div>

        <?php if($_SESSION['error']){
          echo "<div class='bg-red text-lg text-center lato-font, mb-10 p-4'>". $_SESSION['error'] . "</div>";
        }; ?>

      <div class="container mx-auto flex justify-center">

        <div class="w-full max-w-xs">
          <form id="eventform" action="/app/form.php" method="post" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            
            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="username">
                Salutation
              </label>
              <select id="salutation" name="salutation" required class="bg-white shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none" >
                <option value="Mr.">Mr</option>
                <option value="Mrs.">Mrs</option>
                <option value="Miss.">Miss</option>
                <option value="Ms.">Ms</option>
                <option value="Dr.">Dr</option>
                <option value="Prof.">Prof</option>
              </select>
            </div>
            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2"  for="forename">
                Forename
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" 
              id="forename" name="forename" type="text" placeholder="Forename" required>
            </div>
            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="Surname">
                Surname
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" 
              id="surname" name="surname" type="text" placeholder="Surname" required >
            </div>


            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="DOB">
                Date of Birth
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" 
              id="DOB" name="DOB" type="date" placeholder="DOB" required >
            </div>

            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="Telephone">
                Telephone Number
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" 
              id="telephone" name="telephone" type="tel" placeholder="012345678" required >
            </div>

            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="workshopstoattend">
                Workshops to attend
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" 
              id="workshopstoattend" required name="workshopstoattend" type="number" min="0" step="1" placeholder="0">
            </div>
            

            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                Email
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" 
              id="email" name="email" type="email" required placeholder="Email">
            </div>
            <div class="mb-6">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                Password
              </label>
              <input  class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-3 leading-tight focus:outline-none focus:shadow-outline" 
              required id="password" name="password" type="password" placeholder="******************">
            </div>
            <div class="text-center">
              <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                Register
              </button>
            </div>
          </form>
        </div>

      </div>

      <div id="footer" class="p-6 lobster-font text-center">&#169;  Jim Taylor 2018</div>
    
    </body>
    
</html>

