<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.png">

    <title>Event form | Submission result</title>

    <!--TailwindCss-->
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="eventform.css" rel="stylesheet">
    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Lato|Lobster|Playfair+Display: 400,900;" rel="stylesheet">

  </head>
 
    <body class="home-body">

        <div id="page-title" class="playfair-font-black text-3xl text-center mb-10 p-4">
            <?php if($_SESSION['success'] == true):?>
                Thanks for registering. 
                <br>
                <span class="text-lg">Please print out your voucher shown below or bring it with you to the event.
            <?php else: ?>
                Sorry <?php echo $_SESSION['fullName'] ?>, there was a problem with your submission.</span>
                <br>
                <a href="index.php" class="no-underline gray-lighter"> Please try again</a> 
            <?php endif; ?>
        </div>

        <div class="flex">
            <div class="md:w-1/6 lg:w-1/6 xl:w-1/3"></div>
            <div class="w-full m-5 md:w-4/6 lg:w-4/6 xl:w-1/3 flex-col text-center pt-10 h-auto p-20 leading-normal bg-pink-lighter border-dotted border-red border-4">
                    <div class="lobster-font text-4xl mb-2">
                        Voucher
                    </div>
                    <div class="mb-4">
                        This voucher belongs to: 
                        <br>
                        <span class="playfair-font-black">
                        <?php echo $_SESSION['fullName'] ?>
                        </span>
                    </div>
                    <div>
                        Your unique code is: 
                        <br>
                        <span class="italic">
                        <?php echo $_SESSION['voucherCode'] ?>
                        </span>
                    </div>
            </div>
            <div class="md:w-1/6 lg:w-1/6 xl:w-1/3"></div>
        </div>

        <div id="footer" class="p-6 lobster-font text-center">&#169;  Jim Taylor 2018</div>
    
    </body>
    
</html>
