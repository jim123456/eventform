<?php 

require("dbconnect.php");

//Get post data
$formData = $_POST;
$fullName = $_POST['salutation'] . " ".  $_POST['forename'] . " ". $_POST['surname'];
$voucherCode = getVoucherCode();
// Validate post data 
if(validInput()){
    $stmt = prepareStatement($conn,$voucherCode);
} else{
    echo('failed'); //TODO
}

try{
    $save = $stmt->execute();
} catch (Exception $e){
    $message = "Sorry, there was a problem saving your submission, please resubmit the data.";
    $success = false;
}
if($save){
    $message = "Thank you for your submission. Your request has been recorded.";
    $success = true;

}
$stmt->close();
$conn->close();

session_start();
$_SESSION['voucherCode'] = $voucherCode;
$_SESSION['message'] = $message;
$_SESSION['success'] = $success;
$_SESSION['fullName'] = $fullName;


header('Location: public/result.php');

function validInput()
{
    return true;
}

function prepareStatement($conn)
{
    $stmt = $conn
    ->prepare("INSERT INTO UserRegistration 
    (Salutation, 
    Forename, 
    Surname, 
    Dob, 
    Telephone, 
    WorkshopQty, 
    Email, 
    PwdHash, 
    VoucherCode, 
    IP) 
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    $stmt->bind_param("ssssssssss", 
    $_POST["salutation"], $_POST["forename"], $_POST["surname"],
    $_POST["DOB"], $_POST["telephone"], $_POST["workshopstoattend"],
    $_POST["email"],
    getPwdHash(),
    $voucherCode,
    getRealIpAddr());

    return $stmt;
}

function getPwdHash () 
{
    return password_hash($_POST["password"], PASSWORD_DEFAULT); 
}

function getVoucherCode ()
{
    return md5(uniqid(rand(), true));
}

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}